<?php

it('sends a request to the image api', function () {

    $mock = new \GuzzleHttp\Handler\MockHandler([
        new \GuzzleHttp\Psr7\Response(200, ['Content-Length' => 0])
    ]);

    $client = createClient($mock);
    $client->screenshot('https://www.websnap.app');

    expect($mock->getLastRequest()->getUri()->__toString())->toStartWith('https://img.websnap.app');
});

it('creates a client', function () {
    $client = new \Websnap\Php\Client();

    $reflection = new ReflectionClass($client);
    $property = $reflection->getProperty('httpClient');
    $property->setAccessible(true);

    expect($property->getValue($client))->toBeInstanceOf(\GuzzleHttp\Client::class);
});

it('sends a request to the pdf api', function () {

    $mock = new \GuzzleHttp\Handler\MockHandler([
        new \GuzzleHttp\Psr7\Response(200, ['Content-Length' => 0])
    ]);

    $client = createClient($mock);
    $client->pdf('https://www.websnap.app');

    expect($mock->getLastRequest()->getUri()->__toString())->toStartWith('https://pdf.websnap.app');
});

it('sends token as token param', function () {

    $mock = new \GuzzleHttp\Handler\MockHandler([
        new \GuzzleHttp\Psr7\Response(200, ['Content-Length' => 0])
    ]);

    $client = createClient($mock);
    $client->pdf('<h1>Hello World</h1>');

    $request = $mock->getLastRequest();
    parse_str($request->getBody()->getContents(), $params);

    expect($params['token'])->toBe('foobar');
});

it('sends html as html param', function () {

    $mock = new \GuzzleHttp\Handler\MockHandler([
        new \GuzzleHttp\Psr7\Response(200, ['Content-Length' => 0])
    ]);

    $client = createClient($mock);
    $client->pdf('<h1>Hello World</h1>');

    $request = $mock->getLastRequest();
    parse_str($request->getBody()->getContents(), $params);

    expect($params['html'])->not()->toBeEmpty();
});

it('sends url as url param', function () {

    $mock = new \GuzzleHttp\Handler\MockHandler([
        new \GuzzleHttp\Psr7\Response(200, ['Content-Length' => 0])
    ]);

    $client = createClient($mock);
    $client->pdf('https://www.websnap.app');

    $request = $mock->getLastRequest();
    parse_str($request->getBody()->getContents(), $params);

    expect($params['url'])->not()->toBeEmpty();
});
