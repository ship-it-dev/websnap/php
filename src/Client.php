<?php


namespace Websnap\Php;

use GuzzleHttp\Client as GuzzleClient;
use Psr\Http\Message\ResponseInterface;

class Client
{
    public function __construct(
        private string            $token = '',
        private GuzzleClient|null $httpClient = null
    )
    {
        if (!isset($this->httpClient)) {
            $this->httpClient = new GuzzleClient();
        }
    }

    public function pdf(string $source, array $options = []): ResponseInterface
    {

        $options = $this->prepareParams($source, $options);

        return $this->makeRequest('https://pdf.websnap.app', $options);
    }

    private function prepareParams(string $source, array $options = []): array
    {
        $options = array_merge(
            ['token' => $this->token],
            $options
        );

        if (filter_var($source, FILTER_VALIDATE_URL)) {
            $options['url'] = $source;
        } else {
            $options['html'] = $source;
        }

        return $options;
    }

    private function makeRequest(string $url, array $params): ResponseInterface
    {
        // TODO - Wrap in Websnap Exception
        return $this->httpClient->post(
            $url,
            [
                'form_params' => $params
            ]
        );
    }

    public function screenshot(string $source, array $options = []): ResponseInterface
    {
        $options = $this->prepareParams($source, $options);

        return $this->makeRequest('https://img.websnap.app', $options);
    }
}
