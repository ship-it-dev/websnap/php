# Convert HTML to Images/PDFs
[![coverage report](https://gitlab.com/ship-it-dev/websnap/php/badges/master/coverage.svg)](https://gitlab.com/ship-it-dev/websnap/php/-/commits/master)

## Installation
Install with composer: `composer require websnap/php`

## Getting started
1. Register for free at https://www.websnap.app
2. Create a project and copy it's `token`

## Usage
You can either pass an url to a website or a string containing html to the client. The client will return a `\Psr\Http\Message\ResponseInterface` object containing the generated pdf/screenshot.

### Generate an image

```php
$client = new \Websnap\Php\Client('YOUR_TOKEN');

// Take screenshot of an url
$response = $client->screenshot('https://www.websnap.app');

// Take screenshot of passed html
$response = $client->screenshot('<h1>Hello world!</h1>');
```

### Generate a pdf
```php
$client = new \Websnap\Php\Client('YOUR_TOKEN');

// Capture content as PDF
$response = $client->pdf('https://www.websnap.app');

// Generate pdf of passed html
$response = $client->pdf('<h1>Hello world!</h1>');
```
Please see the [documentation](https://www.websnap.app/docs) for further options.

## Contact

Did you found a bug or need a certain feature? Please get in [touch](mailto:support@websnap.app) with us or [raise an issue](https://gitlab.com/ship-it-dev/websnap/php/-/issues/new).